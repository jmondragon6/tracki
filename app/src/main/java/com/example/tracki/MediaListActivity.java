package com.example.tracki;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

public class MediaListActivity extends AppCompatActivity implements PopupMenu.OnMenuItemClickListener{

    private String username;
    private MediaCursorAdapter adapter;

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String u)
    {
        username = u;
    }

    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // get the username
        Bundle extras = getIntent().getExtras();

        // gets old username for updating function
        String username = "";
        if (extras.containsKey("username"))
        {
            setUsername(username);
            username = extras.getString("username");
        }

        // sets content view
        setContentView(R.layout.media_list);

        // creates back button
        ImageView back_button_list = findViewById(R.id.back_button_media_list);

        // get list view
        ListView list = findViewById(R.id.media_list_view);

        // open database
        DBHelper MyDB = new DBHelper(this);

        // set the username again !! THIS WAS CAUSING THE ISSUEEE
        setUsername(username);

        // query to display array however it is unsorted
        Cursor cursor = MyDB.returnMedia(username);

        // creation of adapter
        adapter = new MediaCursorAdapter(username, this, cursor);

        // setting list view to the adapter
        list.setAdapter(adapter);

        // creates on click listener for the item in the list
        String finalUsername1 = username;
        list.setOnItemClickListener((adapterView, view, i, l) -> {
            Cursor cursor_list = (Cursor) adapterView.getItemAtPosition(i);

            // get the data needed for sending with the intent
            String title = cursor_list.getString(cursor_list.getColumnIndexOrThrow("title"));
            String type = cursor_list.getString(cursor_list.getColumnIndexOrThrow("type"));
            String status = cursor_list.getString(cursor_list.getColumnIndexOrThrow("status"));
            String rating = cursor_list.getString(cursor_list.getColumnIndexOrThrow("rating"));
            String notes = cursor_list.getString(cursor_list.getColumnIndexOrThrow("notes"));

            // call intent
            Intent intent = new Intent(MediaListActivity.this, MediaInformation.class);
            intent.putExtra("username", finalUsername1);
            intent.putExtra("title", title);
            intent.putExtra("type", type);
            intent.putExtra("status", status);
            intent.putExtra("rating", rating);
            intent.putExtra("notes", notes);
            startActivity(intent);

        });

        // creates on click listener for the back button
        String finalUsername = username;
        back_button_list.setOnClickListener(view -> {
            Intent intent = new Intent(MediaListActivity.this, HomeScreenActivity.class); // creates new activity to add
            intent.putExtra("username", finalUsername); // adds username intent
            startActivity(intent); // starts activity
        });

    }

    // increasing filter for the list
    public void getIncreasingRating()
    {
        // get list view
        ListView list = findViewById(R.id.media_list_view);

        // open database
        DBHelper MyDB = new DBHelper(this);

        // update the list
        Cursor cursor = MyDB.getIncreasingRating(getUsername());

        // change the cursor
        adapter.changeCursor(cursor);

        // set the new adapter
        list.setAdapter(adapter);

        // close database
        MyDB.close();
    }

    // decreasing filter for the list
    public void getDecreasingOrder()
    {
        // get list view
        ListView list = findViewById(R.id.media_list_view);

        // open database
        DBHelper MyDB = new DBHelper(this);

        // update the list
        Cursor cursor = MyDB.getDecreasingRating(getUsername());

        // change the cursor
        adapter.changeCursor(cursor);

        // set the new adapter
        list.setAdapter(adapter);

        // close database
        MyDB.close();
    }

    public void getTitleOrder()
    {
        // get list view
        ListView list = findViewById(R.id.media_list_view);

        // open database
        DBHelper MyDB = new DBHelper(this);

        // update the list
        Cursor cursor = MyDB.getOrderTitle(getUsername());

        // change the cursor
        adapter.changeCursor(cursor);

        // set the new adapter
        list.setAdapter(adapter);

        // close database
        MyDB.close();
    }

    public void getTypeOrder()
    {
        // get list view
        ListView list = findViewById(R.id.media_list_view);

        // open database
        DBHelper MyDB = new DBHelper(this);

        // update the list
        Cursor cursor = MyDB.getOrderType(getUsername());

        // change the cursor
        adapter.changeCursor(cursor);

        // set the new adapter
        list.setAdapter(adapter);

        // close database
        MyDB.close();
    }

    public void getStatusOrder()
    {
        // get list view
        ListView list = findViewById(R.id.media_list_view);

        // open database
        DBHelper MyDB = new DBHelper(this);

        // update the list
        Cursor cursor = MyDB.getOrderStatus(getUsername());

        // change the cursor
        adapter.changeCursor(cursor);

        // set the new adapter
        list.setAdapter(adapter);

        // close database
        MyDB.close();
    }


    public void showPopup(View v)
    {
        // creates popup
        PopupMenu popup = new PopupMenu(this, v);

        // sets onClick listener
        popup.setOnMenuItemClickListener(this);

        // inflates menu
        popup.inflate(R.menu.popup_menu);

        // show the popup
        popup.show();
    }

    @Override
    public boolean onMenuItemClick(MenuItem menuItem)
    {
        switch(menuItem.getItemId())
        {
            case R.id.ascending_rating:
                // Toast showcasing filter
                Toast.makeText(this, "Sorted by Ascending", Toast.LENGTH_SHORT).show();

                // call function
                getIncreasingRating();

                return true;

            case R.id.descending_rating:
                // Toast showcasing filter
                Toast.makeText(this, "Sorted by Descending", Toast.LENGTH_SHORT).show();

                // call decreasing order
                getDecreasingOrder();

                return true;

            case R.id.rating_title:
                // Toast showcasing filter
                Toast.makeText(this, "Sorted by Title", Toast.LENGTH_SHORT).show();

                // call title order
                getTitleOrder();

                return true;

            case R.id.rating_status:
                // Toast showcasing filter
                Toast.makeText(this, "Sorted by Status", Toast.LENGTH_SHORT).show();

                // call status order
                getStatusOrder();

                return true;

            case R.id.rating_type:
                // Toast showcasing filter
                Toast.makeText(this, "Sorted by Type", Toast.LENGTH_SHORT).show();

                // call type order
                getTypeOrder();

                return true;

            default:
                return false;
        }
    }
}
