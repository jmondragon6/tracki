package com.example.tracki;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.TextView;

public class DBHelper extends SQLiteOpenHelper {

    public static final String DBName = "Tracki.db";

    public DBHelper(Context context) {
        super(context, "Tracki.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase MyDB) {
        // makes user table
        MyDB.execSQL("create Table users(_id integer primary key autoincrement, username TEXT unique, password Text, email Text)");

        // makes media table
        MyDB.execSQL("create Table media(_id integer primary key autoincrement, username TEXT, title TEXT, type TEXT, status TEXT, rating TEXT, notes TEXT)");

        // makes friends table
        MyDB.execSQL("create Table friends(_id integer primary key autoincrement, username TEXT, friend TEXT, status TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase MyDB, int i, int i1) {
        MyDB.execSQL("drop Table if exists users");
        MyDB.execSQL("drop Table if exists media");
        MyDB.execSQL("drop Table if exists friends");
    }

    public Boolean insertData(String username, String password, String email)
    {
        // opens database
        SQLiteDatabase MyDB = this.getWritableDatabase();

        // adds values to be inserted
        ContentValues contentValues = new ContentValues();
        contentValues.put("username", username);
        contentValues.put("password", password);
        contentValues.put("email", email);
        long results = MyDB.insert("users", null, contentValues);

        // returns true or false
        if (results != -1)
        {
            MyDB.close();
            return true;
        }
        else
        {
            MyDB.close();
            return false;
        }
    }

    public Boolean insertMediaData(String username, String title, String type, String status, String rating, String notes)
    {
        // opens database
        SQLiteDatabase MyDB = this.getWritableDatabase();

        // adds values to be inserted
        ContentValues contentValues = new ContentValues();

        // adds to content values
        contentValues.put("username", username);
        contentValues.put("title", title);
        contentValues.put("type", type);
        contentValues.put("status", status);
        contentValues.put("rating", rating);
        contentValues.put("notes", notes);

        // creates query
        long result = MyDB.insertWithOnConflict("media", null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);

        // returns results
        if (result != -1)
        {
            MyDB.close();
            return true;
        }
        else
        {
            MyDB.close();
            return false;
        }
    }

    public Boolean checkUsername(String username)
    {
        // opens database
        SQLiteDatabase MyDB = this.getWritableDatabase();

        // sets cursor
        Cursor cursor = MyDB.rawQuery("Select * from users where username = ?", new String[] {username});

        // return true or false
        if (cursor.getCount() > 0)
        {
            MyDB.close();
            return true;
        }
        else
        {
            MyDB.close();
            return false;
        }
    }

    public Boolean checkUsernamePassword(String username, String password)
    {
        // open the database
        SQLiteDatabase MyDB = this.getWritableDatabase();

        // set cursor
        Cursor cursor = MyDB.rawQuery("Select * from users where username = ? and password = ?", new String[] {username, password});

        // return true or false
        if (cursor.getCount() > 0)
        {
            MyDB.close();
            return true;
        }
        else
        {
            MyDB.close();
            return false;
        }
    }

    public void updateAccount(String old_username, String username, String password, String email)
    {
        // update the account information
        SQLiteDatabase MyDB = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put("username", username);
        contentValues.put("password", password);
        contentValues.put("email", email);

        // used to update the database tables
        MyDB.update("users", contentValues,"username=?", new String[]{old_username});

        //update the rest of the tables
        ContentValues other = new ContentValues();
        other.put("username", username);
        MyDB.update("media", other,"username=?", new String[]{old_username});
        MyDB.update("friends", other,"username=?", new String[]{old_username});

        // close the database
        MyDB.close();
    }

    public void updateMedia(String username, String old_media_title, String media_title, String media_type, String media_status, String media_rating, String media_notes)
    {
        // open the database to update the media
        SQLiteDatabase MyDB = this.getWritableDatabase();

        // Add values to a contentValues
        ContentValues contentValues = new ContentValues();
        contentValues.put("title", media_title);
        contentValues.put("type", media_type);
        contentValues.put("status", media_status);
        contentValues.put("rating", media_rating);
        contentValues.put("notes", media_notes);

        // update call
        MyDB.update("media", contentValues, "username=? and title=?", new String[]{username, old_media_title});

        // close the database
        MyDB.close();
    }

    public void deleteMedia(String username, String media_title)
    {
        // open the database
        SQLiteDatabase MyDB = this.getWritableDatabase();

        // delete the row from the database
        MyDB.delete("media","username=? and title=?", new String[]{username, media_title});

        // close the database
        MyDB.close();
    }

    public String[] findUser(String username)
    {
        // opens db
        SQLiteDatabase MyDB = this.getReadableDatabase();

        // creates query
        Cursor cursor = MyDB.rawQuery("select username, password, email from users where username = ?", new String[] {username});

        // creates array to hold the data
        String[] data = null;

        if (cursor.moveToFirst())
        {
            data = new String[cursor.getColumnCount()];
            for (int i = 0; i < cursor.getColumnCount(); i++)
            {
                if(!cursor.isNull(i))
                {
                    data[i] = cursor.getString(i);
                }
            }
        }
        MyDB.close();
        return data;
    }

    public int countEntries(String username)
    {
        // creates the readable database
        SQLiteDatabase MyDB = this.getReadableDatabase();

        // creates the query
        Cursor cursor = MyDB.rawQuery("select count(*) from media where username='" + username + "'", null);
        cursor.moveToFirst();

        // gets count of entries
        int count = cursor.getInt(0);

        // closes database
        MyDB.close();

        // returns count
        return count;
    }

    // fake accounts (from text file) have been implemented in order to showcase the friend list functionality
    /*public void insertUser() throws FileNotFoundException {
        SQLiteDatabase MyDB = this.getWritableDatabase();

        // adding each user from the text file
        File file = new File("fake_user.txt");
        Scanner scanner = new Scanner(file);

        while(scanner.hasNextLine())
        {
            // reads the line
            String line = scanner.nextLine();

            // reads the data
            String[] data = line.split(" ");

            // adds the data to a contentValue
            ContentValues account = new ContentValues();
            account.put("username", data[0]);
            account.put("email", data[1]);
            account.put("password", data[2]);

            // adds to the database
            MyDB.insert("users", null, account);
        }

        scanner.close();
        MyDB.close();
    }*/

    // returns the cursor to the cursor for increasing ratings
    public Cursor getIncreasingRating(String username)
    {
        // open the database
        SQLiteDatabase MyDB = this.getReadableDatabase();

        // create the cursor
        Cursor cursor = MyDB.rawQuery("SELECT _id, title, type, status, rating, notes FROM media WHERE username = ? ORDER BY CAST(rating AS INTEGER) ASC", new String[]{username});

        // return the cursor
        return cursor;
    }

    // returns the cursor to the cursor for decreasing ratings
    public Cursor getDecreasingRating(String username)
    {
        // open the database
        SQLiteDatabase MyDB = this.getReadableDatabase();

        // create the cursor
        Cursor cursor = MyDB.rawQuery("SELECT _id, title, type, status, rating, notes FROM media WHERE username = ? ORDER BY CAST(rating AS INTEGER) DESC", new String[]{username});

        // return the cursor
        return cursor;
    }

    // returns the cursor to the query for title order
    public Cursor getOrderTitle(String username)
    {
        // open the database
        SQLiteDatabase MyDB = this.getReadableDatabase();

        // create cursor
        Cursor cursor = MyDB.rawQuery("SELECT _id, title, type, status, rating, notes FROM media WHERE username = ? ORDER BY title ASC", new String[]{username});

        // return the cursor
        return cursor;
    }

    // returns the cursor to the query for type order
    public Cursor getOrderType(String username)
    {
        // open database
        SQLiteDatabase MyDB = this.getReadableDatabase();

        // create cursor
        Cursor cursor = MyDB.rawQuery("SELECT _id, title, type, status, rating, notes FROM media WHERE username = ? ORDER BY type ASC", new String[]{username});

        // return the cursor
        return cursor;
    }

    // returns the cursor to the query for status order
    public Cursor getOrderStatus(String username)
    {
        // open the database
        SQLiteDatabase MyDB = this.getReadableDatabase();

        // create cursor
        Cursor cursor = MyDB.rawQuery("SELECT _id, title, type, status, rating, notes FROM media WHERE username = ? ORDER BY status ASC", new String[]{username});

        // return the cursor
        return cursor;
    }

    // returns all the media under one username
    public Cursor returnMedia(String username)
    {
        // open the database
        SQLiteDatabase MyDB = this.getReadableDatabase();

        // create the cursor
        Cursor cursor = MyDB.rawQuery("SELECT _id, title, type, status, rating, notes FROM media WHERE username = ?", new String[]{username});

        // return the cursor
        return cursor;
    }

    // deletes all the media entries under one username
    public void deleteAllMedia(String username)
    {
        // opens database
        SQLiteDatabase MyDB = this.getWritableDatabase();

        // deletes all media entries from the media table
        MyDB.delete("media", "username=?", new String[]{username});

        // close the database
        MyDB.close();
    }

    // deletes the account based off the username
    public void deleteAccount(String username)
    {
        // opens database
        SQLiteDatabase MyDB = this.getWritableDatabase();

        // deletes account from the user table
        MyDB.delete("users", "username=?", new String[]{username});

        // close the database
        MyDB.close();
    }

    // removes the friend request or friend based on the passed username and friend
    public void removeFriend(String username, String friend)
    {
        // open the database
        SQLiteDatabase MyDB = this.getWritableDatabase();

        // deletes the request from the friend table
        MyDB.delete("friends", "username=? and friend=?", new String[]{username, friend});

        // delete the other way as well
        // deletes the request from the friend table
        MyDB.delete("friends", "username=? and friend=?", new String[]{friend, username});

        // close the database
        MyDB.close();
    }

    // add the user to the friend list
    public boolean addFriend(String username, String friend, String status)
    {
        // open the database
        SQLiteDatabase MyDB = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put("username", username);
        contentValues.put("friend", friend);
        contentValues.put("status", status);
        long results_first = MyDB.insert("friends", null, contentValues);

        ContentValues contentValues2 = new ContentValues();
        contentValues2.put("username", friend);
        contentValues2.put("friend", username);
        contentValues2.put("status", status);
        long results_second = MyDB.insert("friends", null, contentValues2);

        if (results_first != -1 && results_second != -1)
        {
            MyDB.close();
            return true;
        }
        else
        {
            MyDB.close();
            return false;
        }
    }

    // essentially updates the status to where the text will say pending
    public void updateFriend(String username, String friend, String new_status)
    {
        // open the database
        SQLiteDatabase MyDB = this.getWritableDatabase();

        // updates the request in the friend table
        ContentValues contentValues = new ContentValues();
        contentValues.put("status", new_status);

        MyDB.update("friends", contentValues, "username=? and friend=?", new String[]{username, friend});
        MyDB.update("friends", contentValues, "username=? and friend=?", new String[]{friend, username});

        // close the database
        MyDB.close();
    }

    // should return all friends for a user
    public Cursor returnFriendList(String username, String status)
    {
        // open the database
        SQLiteDatabase MyDB = this.getReadableDatabase();

        // create the cursor
        Cursor cursor = MyDB.rawQuery("SELECT _id, username, friend, status FROM friends WHERE username=? and status=?", new String[]{username, status});

        // return the cursor
        return cursor;
    }

    // finds the username
    public Cursor findUsername(String s)
    {
        // open the database
        SQLiteDatabase MyDB = this.getReadableDatabase();

        // create the cursor
        Cursor cursor = MyDB.rawQuery("SELECT username, _id FROM users where username like ?", new String[]{"%" + s + "%"});

        // return the cursor
        return cursor;
    }
}
