package com.example.tracki;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MediaActivity extends AppCompatActivity {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // sets content view
        setContentView(R.layout.add_media);

        // username intent
        // get the username
        Bundle extras = getIntent().getExtras();

        String username = "";
        if (extras.containsKey("username")) {
            username = extras.getString("username");
        }

        // sets back button -> sends to home
        ImageView home_button = findViewById(R.id.back_button_media);

        // add button -> add media to database
        Button add_media_btn = findViewById(R.id.add_media_btn);

        // sets variables for media add
        EditText mediaTitle = findViewById(R.id.media_title);
        Spinner mediaType = findViewById(R.id.media_spinner);
        RadioGroup radioGroup = findViewById(R.id.media_status);
        Spinner mediaRating = findViewById(R.id.media_rating_bar);
        EditText mediaComments = findViewById(R.id.media_comments);

        // SQLite Database
        DBHelper MediaTracker = new DBHelper(this);

        // on-click listener for home button
        String finalUsername1 = username;
        home_button.setOnClickListener(view -> {
            Intent intent = new Intent(MediaActivity.this, HomeScreenActivity.class);
            intent.putExtra("username", finalUsername1); // adds username intent
            finish();
        });

        // on-click listener for adding media button
        String finalUsername = username;
        add_media_btn.setOnClickListener(view -> {
            String title = mediaTitle.getText().toString();
            String type = mediaType.getSelectedItem().toString();

            int selectedRadioButton = radioGroup.getCheckedRadioButtonId();
            RadioButton selected = findViewById(selectedRadioButton);

            String status = "";

            //check radio button because it was giving me trouble
            if (selected != null)
            {
                status = selected.getText().toString();
            }
            else
            {
                status = "";
            }

            String rating = mediaRating.getSelectedItem().toString();
            String notes = mediaComments.getText().toString();

            // checks for empty fields
            if (title.isEmpty() || type.isEmpty() || status.isEmpty() || rating.isEmpty())
            {
                Toast.makeText(MediaActivity.this, "Required Fields Empty", Toast.LENGTH_SHORT).show();
            }
            else // starts SQL query
            {
                // adds media to database
                if (notes.isEmpty())
                {
                    notes = "-";
                }
                Boolean insert = MediaTracker.insertMediaData(finalUsername, title, type, status, rating, notes);
                if (insert)
                {
                    // lets the user now that the media has been entered
                    Toast.makeText(MediaActivity.this, title + " Added", Toast.LENGTH_SHORT).show();

                    // resets text to empty
                    mediaTitle.setText(""); // sets text
                    mediaType.setSelection(0); // sets spinner
                    mediaRating.setSelection(0); // sets spinner
                    mediaComments.setText(""); // sets comment
                    radioGroup.clearCheck();
                }
                else
                {
                    // if a media already exists matching the input, displays it is already inserted
                    Toast.makeText(MediaActivity.this, "Media Not Added", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
