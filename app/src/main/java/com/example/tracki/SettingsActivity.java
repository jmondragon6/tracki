package com.example.tracki;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Switch;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

public class SettingsActivity extends AppCompatActivity {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // sets content view
        setContentView(R.layout.open_settings);

        // creates elements to interact with
        RelativeLayout logout_layout = findViewById(R.id.logout_layout);
        RelativeLayout profile_layout = findViewById(R.id.profile_layout);
        ImageView home_button = findViewById(R.id.back_button_settings);
        ImageView delete_data = findViewById(R.id.delete_data_button);
        ImageView delete_account = findViewById(R.id.delete_account_button);

        // get username to pass on to intent
        // get the username
        Bundle extras = getIntent().getExtras();

        // gets old username for updating function
        String old_username = "";
        if (extras.containsKey("username")) {
            old_username = extras.getString("username");
        }

        // sets final username to pass for the intent
        String finalOld_username = old_username;

        // SQLite Database
        DBHelper MediaTracker = new DBHelper(this);

        // sets home button onClick listener
        home_button.setOnClickListener(view -> {
            Intent intent = new Intent(SettingsActivity.this, HomeScreenActivity.class);
            intent.putExtra("username", finalOld_username);
            startActivity(intent);
        });

        // sets logout button (sends to home menu)
        logout_layout.setOnClickListener(view -> {
            Intent intent = new Intent(SettingsActivity.this, MainActivity.class);
            startActivity(intent);
        });

        // sends user to the profile page
        profile_layout.setOnClickListener(view -> {
            Intent intent = new Intent(SettingsActivity.this, ProfileActivity.class);
            intent.putExtra("username", finalOld_username);
            startActivity(intent);
        });

        // sets delete all data button
        delete_data.setOnClickListener(view -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(SettingsActivity.this);

            builder.setMessage("Are you sure you would like to delete all the entries on your account?")
                    .setCancelable(true)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            // deletion code which calls the database
                            MediaTracker.deleteAllMedia(finalOld_username);
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();
                        }
                    });

                // show the alert dialogue
                AlertDialog alert = builder.create();
                alert.show();
        });

        // sets delete all data button
        delete_account.setOnClickListener(view -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(SettingsActivity.this);

            builder.setMessage("Are you sure you would like to delete your account?")
                    .setCancelable(true)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            // deletion code which calls the database
                            MediaTracker.deleteAllMedia(finalOld_username);

                            // deletion code to remove account
                            MediaTracker.deleteAccount(finalOld_username);

                            // starts new intent
                            Intent intent = new Intent(SettingsActivity.this, MainActivity.class);
                            startActivity(intent);
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();
                        }
                    });

            // show the alert dialogue
            AlertDialog alert = builder.create();
            alert.show();
        });
    }

}
