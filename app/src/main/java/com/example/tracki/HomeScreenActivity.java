package com.example.tracki;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class HomeScreenActivity extends AppCompatActivity {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Creates variables along with the home screen media that can be used
        // to click on
        LinearLayout AddMedia = findViewById(R.id.layout_AddMedia);
        LinearLayout ViewList = findViewById(R.id.layout_list);
        LinearLayout Friends = findViewById(R.id.layout_social);
        LinearLayout Settings = findViewById(R.id.layout_settings);

        // get the username
        Bundle extras = getIntent().getExtras();

        String username = "";
        if (extras.containsKey("username")) {
            username = extras.getString("username");
        }

        // finds username from database so the intents stop messing up
        DBHelper MyDB = new DBHelper(this);

        // finds the user information
        String[] data = MyDB.findUser(username);

        // sets text boxes to the data pulled from the database
        for (int i = 0; i < data.length; i++)
        {
            if (i == 0) {username = data[0];}
        }

        // set welcome text view to username from login system
        TextView welcome = findViewById(R.id.username_home);
        welcome.setText("Welcome, " + username + "!");

        // on click method to add media to the user account
        String finalUsername = username;
        AddMedia.setOnClickListener(view -> {
            // sends to add media page
            Intent intent = new Intent(HomeScreenActivity.this, MediaActivity.class);
            intent.putExtra("username", finalUsername);
            startActivity(intent);
        });

        ViewList.setOnClickListener(view -> {
            // sends to list page
            Intent intent = new Intent(HomeScreenActivity.this, MediaListActivity.class);
            intent.putExtra("username", finalUsername);
            startActivity(intent);
        });

        Friends.setOnClickListener(view -> {
            // sends to friends lists
            Intent intent = new Intent(HomeScreenActivity.this, FriendsActivity.class);
            intent.putExtra("username", finalUsername);
            startActivity(intent);
        });

        Settings.setOnClickListener(view -> {
            // sends to setting page
            Intent intent = new Intent(HomeScreenActivity.this, SettingsActivity.class);
            intent.putExtra("username", finalUsername);
            startActivity(intent);
        });
    }
}
