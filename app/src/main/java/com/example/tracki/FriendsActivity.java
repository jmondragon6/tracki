package com.example.tracki;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

public class FriendsActivity extends AppCompatActivity {
    private FriendCursorAdapter adapter;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.friends_list);

        // get the username
        Bundle extras = getIntent().getExtras();

        String username = "";
        if (extras.containsKey("username")) {
            username = extras.getString("username");
        }

        // creates buttons for onClickListeners
        ImageView home_button = findViewById(R.id.back_button_friend);
        ImageView add_friend = findViewById(R.id.add_friend_button);

        // open database
        DBHelper MyDB = new DBHelper(this);

        // get list view
        ListView list = findViewById(R.id.full_friend_list);

        // fake data for testing
        // MyDB.addFriend(username, "Amethyst", "added");

        // query to display array however it is unsorted
        Cursor cursor = MyDB.returnFriendList(username, "added");

        // creation of adapter
        adapter = new FriendCursorAdapter(username, this, cursor);

        // setting list view to the adapter
        list.setAdapter(adapter);

        // home button on click
        String finalUsername = username;
        home_button.setOnClickListener(view -> {
            Intent intent = new Intent(FriendsActivity.this, HomeScreenActivity.class);
            intent.putExtra("username", finalUsername);
            startActivity(intent);
        });

        // add friend onClick
        add_friend.setOnClickListener(view -> {
            Intent intent = new Intent(FriendsActivity.this, AddFriendActivity.class);
            intent.putExtra("username", finalUsername);
            startActivity(intent);
        });

        // set on click listener for the list
        list.setOnItemClickListener((adapterView, view, i, l) -> {
            Cursor cursor_list = (Cursor) adapterView.getItemAtPosition(i);

            // get the data needed for sending with the intent
            String friend = cursor_list.getString(cursor_list.getColumnIndexOrThrow("friend"));

            // call intent
            Intent intent = new Intent(FriendsActivity.this, FriendProfile.class);
            intent.putExtra("username", finalUsername);
            intent.putExtra("friend", friend);
            startActivity(intent);
        });

    }
}
