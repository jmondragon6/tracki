package com.example.tracki;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SearchView;

import androidx.appcompat.app.AppCompatActivity;

public class AddFriendActivity extends AppCompatActivity
{
   private FriendAddCursorAdapter adapter;
   private SearchFriendCursorAdapter adapter1;

    public void onCreate(Bundle savedInstanceState)
    {
        // creates view
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_friend);

        // finds the objects in the add_friend.xml
        findViewById(R.id.results_friend_list);
        DBHelper MyDB = new DBHelper(this);

        // creates Image View for onClickListener
        ImageView home_button = findViewById(R.id.back_button_friend_add);

        // gets the username
        // get the username
        Bundle extras = getIntent().getExtras();

        String username = "";
        if (extras.containsKey("username")) {
            username = extras.getString("username");
        }

        // get list view
        ListView list = findViewById(R.id.results_friend_list);

        // get the search view
        SearchView search = findViewById(R.id.search_bar);

        // fake data to add friend
        // MyDB.addFriend(username, "Amethyst", "pending");

        // query to display array however it is unsorted
        Cursor cursor = MyDB.returnFriendList(username, "pending");

        // creation of adapter
        adapter = new FriendAddCursorAdapter(username, this, cursor);

        // setting list view to the adapter
        list.setAdapter(adapter);

        // giving issues
        adapter1 = new SearchFriendCursorAdapter(username, this, cursor);

        //onClick listener
        String finalUsername = username;
        home_button.setOnClickListener(view -> {
            Intent intent = new Intent(AddFriendActivity.this, FriendsActivity.class);
            intent.putExtra("username", finalUsername);
            startActivity(intent);
        });

        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {

                // cursor for the data base
                Cursor cursor = MyDB.findUsername(s);

                // set up adapter for the list view
                adapter1.changeCursor(cursor);

                // get list view
                list.setAdapter(adapter1);

                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                // waits for user to click submit
                return false;
            }
        });
    }
}
