package com.example.tracki;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class FriendAddCursorAdapter extends CursorAdapter {

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    private String username;

    public FriendAddCursorAdapter(String username, Context context, Cursor cursor)
    {
        super(context, cursor,  0);
        setUsername(username);
    }
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup)
    {
        // creates inflater
        LayoutInflater inflate = LayoutInflater.from(context);

        // sets the view
        View view = inflate.inflate(R.layout.pending_friend_item, viewGroup, false);

        // returns the view
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor)
    {
        // get string from column
        String friend = cursor.getString(cursor.getColumnIndexOrThrow("friend"));
        String status = cursor.getString(cursor.getColumnIndexOrThrow("status"));

        // set the views for the text
        TextView friend_name = view.findViewById(R.id.pending_add_friend_text);
        ImageView decline_button = view.findViewById(R.id.decline_friend_button);
        ImageView accept_button = view.findViewById(R.id.accept_friend_button);

        // open database
        DBHelper MyDB = new DBHelper(context);

        // for the status (if pending show delete button), else, display only add
        if("pending".equals(status))
        {
            accept_button.setVisibility(view.VISIBLE);
            decline_button.setVisibility(view.VISIBLE);
        }
        else
        {
            accept_button.setVisibility(view.VISIBLE);
            decline_button.setVisibility(view.GONE);
        }

        // set the item
        friend_name.setText(friend);

        // onClick listener for the decline button
        decline_button.setOnClickListener(view1 ->
        {
            // call the query to remove the request from the database
            MyDB.removeFriend(getUsername(), friend);

            // show toast indicated declining add
            Toast.makeText(context, "Declined Friend Request", Toast.LENGTH_SHORT).show();

            // make them invisible so they disappear
            friend_name.setVisibility(view1.INVISIBLE);
            decline_button.setVisibility(view1.INVISIBLE);
            accept_button.setVisibility(view1.INVISIBLE);
        });

        accept_button.setOnClickListener(view1 -> {
            // set status to "added"
            String new_status = "added";

            // call the query to remove the request from the database and add user
            MyDB.updateFriend(getUsername(), friend, new_status);

            // show toast indicated declining add
            Toast.makeText(context, "Accepted Friend Request", Toast.LENGTH_SHORT).show();

            // make them invisible so they disappear
            friend_name.setVisibility(view1.INVISIBLE);
            decline_button.setVisibility(view1.INVISIBLE);
            accept_button.setVisibility(view1.INVISIBLE);
        });
    }
}
