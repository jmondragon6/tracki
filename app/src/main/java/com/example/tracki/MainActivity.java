package com.example.tracki;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_fragment);

        EditText username_text = findViewById(R.id.login_username_text);
        EditText password_text = findViewById(R.id.login_password_text);

        Button new_user_button = findViewById(R.id.new_user_button);
        new_user_button.setOnClickListener(view -> {
            Intent intent = new Intent(MainActivity.this, CreateUserActivity.class);
            startActivity(intent);
        });

        Button login_button = findViewById(R.id.login_button);
        DBHelper LoginSystem = new DBHelper(this);

        login_button.setOnClickListener(view -> {

            // creates variables to hold the username and password
            String username, password;
            // pulls the username and password from the login menu
            username = username_text.getText().toString();
            password = password_text.getText().toString();

            // checks if the username or password is empty
            if (username.isEmpty() || password.isEmpty())
            {
                Toast.makeText(MainActivity.this, "Please Enter Username/Password", Toast.LENGTH_SHORT).show();
            }
            // here is where we will check SQLite for Account
            else
            {
                Boolean checker = LoginSystem.checkUsernamePassword(username, password);

                if (checker)
                {
                    Intent intent = new Intent(MainActivity.this, HomeScreenActivity.class);
                    intent.putExtra("username", username);
                    startActivity(intent);
                }

                else
                {
                    Toast.makeText(MainActivity.this, "Invalid Username/Password", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}

