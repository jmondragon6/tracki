package com.example.tracki;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class FriendCursorAdapter extends CursorAdapter {

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    private String username;

    public FriendCursorAdapter(String username, Context context, Cursor cursor)
    {
        super(context, cursor,  0);
        setUsername(username);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup)
    {
        // creates inflater
        LayoutInflater inflate = LayoutInflater.from(context);

        // sets the view
        View view = inflate.inflate(R.layout.friend_item, viewGroup, false);

        // returns the view
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor)
    {
        // get strings
        String friend = cursor.getString(cursor.getColumnIndexOrThrow("friend"));

        // set the views for the text
        TextView friend_name = view.findViewById(R.id.list_friend_text);

        // set the item
        friend_name.setText(friend);
    }
}
