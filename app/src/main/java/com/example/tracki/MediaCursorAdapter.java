package com.example.tracki;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

public class MediaCursorAdapter extends CursorAdapter {

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    private String username;

    public MediaCursorAdapter(String username, Context context, Cursor cursor)
    {
        super(context, cursor,  0);
        setUsername(username);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {

        // creates inflater
        LayoutInflater inflate = LayoutInflater.from(context);

        // sets the view
        View view = inflate.inflate(R.layout.media_item, viewGroup, false);

        // returns the view
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        // get string
        String title = cursor.getString(cursor.getColumnIndexOrThrow("title"));
        String type = cursor.getString(cursor.getColumnIndexOrThrow("type"));
        String status = cursor.getString(cursor.getColumnIndexOrThrow("status"));
        String rating = cursor.getString(cursor.getColumnIndexOrThrow("rating"));
        String notes = cursor.getString(cursor.getColumnIndexOrThrow("notes"));

        // Get variables
        TextView title_text = view.findViewById(R.id.text_title);
        TextView type_text = view.findViewById(R.id.text_type);
        TextView status_text = view.findViewById(R.id.text_status);
        TextView rating_text = view.findViewById(R.id.text_rating);
        TextView notes_text = view.findViewById(R.id.text_notes);

        // set the item
        title_text.setText(title);
        type_text.setText(type);
        status_text.setText(status);
        rating_text.setText(rating);
        notes_text.setText(notes);
    }
}