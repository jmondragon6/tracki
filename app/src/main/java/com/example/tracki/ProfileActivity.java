package com.example.tracki;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;

public class ProfileActivity extends AppCompatActivity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // sets content view
        setContentView(R.layout.profile_page);

        // get the username
        Bundle extras = getIntent().getExtras();

        String username = "";
        if (extras.containsKey("username")) {
            username = extras.getString("username");
        }

        // creates buttons
        Button update_btn = findViewById(R.id.update_account);
        ImageView home_button_profile = findViewById(R.id.back_button_profile);

        // variables
        TextView username_profile = findViewById(R.id.username_profile);
        TextView media_count = findViewById(R.id.media_count);
        TextInputEditText username_update = findViewById(R.id.profile_update_username);
        TextInputEditText email_update = findViewById(R.id.profile_update_email);
        TextInputEditText password_update = findViewById(R.id.profile_update_password);

        // sets username from intent to profile name
        username_profile.setText(username);

        // database for user information
        DBHelper LoginSystem = new DBHelper(this);

        // set media count to the number of entries in the database
        int count  = LoginSystem.countEntries(username);
        media_count.setText(String.valueOf(count));

        // finds the user information
        String[] data = LoginSystem.findUser(username);

        // sets text boxes to the data pulled from the database
        for (int i = 0; i < data.length; i++)
        {
            if (i == 0) {username_update.setText(data[0]);}
            if (i == 1) {password_update.setText(data[1]);}
            if (i == 2) {email_update.setText(data[2]);}
        }

        String finalOld_username = username;
        update_btn.setOnClickListener(view -> {
            // gets updated information
            String new_username = username_update.getText().toString();
            String new_email = email_update.getText().toString();
            String new_password = password_update.getText().toString();

            // function to update account information
            LoginSystem.updateAccount(finalOld_username, new_username, new_password, new_email);

            // restarts the activity to update with the changes
            Intent intent = new Intent(ProfileActivity.this, ProfileActivity.class);
            intent.putExtra("username", new_username);
            startActivity(intent);
        });

        home_button_profile.setOnClickListener(view -> {
            Intent intent = new Intent(ProfileActivity.this, HomeScreenActivity.class);
            intent.putExtra("username", finalOld_username);
            startActivity(intent);
        });
    }
}
