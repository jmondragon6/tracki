package com.example.tracki;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class CreateUserActivity extends AppCompatActivity {

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_account);

        Button login_button = findViewById(R.id.create_login_button);
        ImageView back_button = findViewById(R.id.back_button_create);
        DBHelper LoginSystem = new DBHelper(this);

        login_button.setOnClickListener(view -> {
            EditText email = findViewById(R.id.create_email_text);
            EditText username = findViewById(R.id.create_user_text);
            EditText password = findViewById(R.id.create_password_text);
            EditText repass = findViewById(R.id.confirm_password_text);

            String email_text = email.getText().toString();
            String username_text = username.getText().toString();
            String password_text = password.getText().toString();
            String confirm_text = repass.getText().toString();

            if (email_text.isEmpty() || username_text.isEmpty() || password_text.isEmpty() || confirm_text.isEmpty())
            {
                Toast.makeText(CreateUserActivity.this, "Empty Fields", Toast.LENGTH_SHORT).show();
            }
            else
            {
                // add user information to the SQLite Database
                if(password_text.equals(confirm_text))
                {
                    Boolean check = LoginSystem.checkUsername(username_text);
                    if(!false)
                    {
                        Boolean insert = LoginSystem.insertData(username_text, password_text, email_text);
                        if (insert)
                        {
                            Intent intent = new Intent(CreateUserActivity.this, HomeScreenActivity.class);
                            intent.putExtra("username", username_text);
                            startActivity(intent);
                        }
                        else
                        {
                            Toast.makeText(CreateUserActivity.this, "Email Already Registered", Toast.LENGTH_SHORT).show();
                        }
                    }
                    else
                    {
                        Toast.makeText(CreateUserActivity.this, "Username Taken", Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(CreateUserActivity.this, "Passwords Don't Match", Toast.LENGTH_SHORT).show();
                }
            }
        });

        back_button.setOnClickListener(view -> {
            startActivity(new Intent(CreateUserActivity.this, MainActivity.class));
            finish();
        });

    }
}
