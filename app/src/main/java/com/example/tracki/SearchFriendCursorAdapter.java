package com.example.tracki;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class SearchFriendCursorAdapter extends CursorAdapter
{

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    private String username;

    public SearchFriendCursorAdapter(String username, Context context, Cursor cursor)
    {
        super(context, cursor,  0);
        setUsername(username);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup)
    {
        // creates inflater
        LayoutInflater inflate = LayoutInflater.from(context);

        // sets the view
        View view = inflate.inflate(R.layout.add_friend_item, viewGroup, false);

        // returns the view
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        // get string from column
        String friend = cursor.getString(cursor.getColumnIndexOrThrow("username"));

        // set the views for the text
        TextView username_text = view.findViewById(R.id.add_friend_text_item);
        ImageView add_button = view.findViewById(R.id.add_friend_button_item);

        // open database
        DBHelper MyDB = new DBHelper(context);

        // set the item
        if (friend.equals(getUsername()))
        {
            username_text.setText(getUsername());
        }

        username_text.setText(friend);

        add_button.setOnClickListener(view1 -> {
            // set status to "added"
            String new_status = "pending";

            // call the query to remove the request from the database and add user
            MyDB.addFriend(getUsername(), friend, new_status);

            // show toast indicated declining add
            Toast.makeText(context, "Sent Friend Request", Toast.LENGTH_SHORT).show();

            // make them invisible so they disappear
            username_text.setVisibility(view1.INVISIBLE);
            add_button.setVisibility(view1.INVISIBLE);
        });
    }
}
