package com.example.tracki;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.media.Image;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class FriendProfile  extends AppCompatActivity implements PopupMenu.OnMenuItemClickListener{

    private String username;

    public String getFriend() {
        return friend;
    }

    public void setFriend(String friend) {
        this.friend = friend;
    }

    private String friend;
    private MediaCursorAdapter adapter;

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String u)
    {
        username = u;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // sets content view
        setContentView(R.layout.friend_profile);

        // get the username
        Bundle extras = getIntent().getExtras();

        String username = "";
        String friend = "";
        if (extras.containsKey("username")) {
            username = extras.getString("username");
        }

        if (extras.containsKey("friend")) {
            friend = extras.getString("friend");
        }

        // creates back button
        ImageView back_button = findViewById(R.id.back_button_friend_profile);
        ImageView delete_friend = findViewById(R.id.delete_friend_profile_view);

        // get list view
        ListView list = findViewById(R.id.friend_list_view);

        // set text view for media count & username
        TextView media_count = findViewById(R.id.media_count_friend);
        TextView friend_username = findViewById(R.id.friend_username_profile);

        // open database
        DBHelper MyDB = new DBHelper(this);

        // set the username again !! THIS WAS CAUSING THE ISSUEEE
        setUsername(username);
        setFriend(friend);

        // query to display array however it is unsorted
        Cursor cursor = MyDB.returnMedia(friend);

        // creation of adapter
        adapter = new MediaCursorAdapter(friend, this, cursor);

        // setting list view to the adapter
        list.setAdapter(adapter);

        // set media count to the number of entries in the database & set text
        int count  = MyDB.countEntries(friend);
        media_count.setText(String.valueOf(count));
        friend_username.setText(friend);

        // sets final
        String finalUsername = username;

        // sets delete button onClick listener
        String finalFriend = friend;
        delete_friend.setOnClickListener(view -> {

            AlertDialog.Builder builder = new AlertDialog.Builder(FriendProfile.this);
            builder.setMessage("Want to delete this entry?")
                    .setCancelable(true)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            // deletion code to remove account
                            MyDB.removeFriend(finalUsername, finalFriend);

                            // starts new intent
                            Intent intent = new Intent(FriendProfile.this, FriendsActivity.class);
                            intent.putExtra("username", finalUsername);
                            startActivity(intent);
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();
                        }
                    });

            // show the alert dialogue
            AlertDialog alert = builder.create();
            alert.show();
        });

        // sets back button onClick listener
        back_button.setOnClickListener(view -> {
            Intent intent = new Intent(FriendProfile.this, FriendsActivity.class);
            intent.putExtra("username", finalUsername);
            startActivity(intent);
        });
    }

    // increasing filter for the list
    public void getIncreasingRating()
    {
        // get list view
        ListView list = findViewById(R.id.friend_list_view);

        // open database
        DBHelper MyDB = new DBHelper(this);

        // update the list
        Cursor cursor = MyDB.getIncreasingRating(getUsername());

        // change the cursor
        adapter.changeCursor(cursor);

        // set the new adapter
        list.setAdapter(adapter);

        // close database
        MyDB.close();
    }

    // decreasing filter for the list
    public void getDecreasingOrder()
    {
        // get list view
        ListView list = findViewById(R.id.friend_list_view);

        // open database
        DBHelper MyDB = new DBHelper(this);

        // update the list
        Cursor cursor = MyDB.getDecreasingRating(getUsername());

        // change the cursor
        adapter.changeCursor(cursor);

        // set the new adapter
        list.setAdapter(adapter);

        // close database
        MyDB.close();
    }

    public void getTitleOrder()
    {
        // get list view
        ListView list = findViewById(R.id.friend_list_view);

        // open database
        DBHelper MyDB = new DBHelper(this);

        // update the list
        Cursor cursor = MyDB.getOrderTitle(getUsername());

        // change the cursor
        adapter.changeCursor(cursor);

        // set the new adapter
        list.setAdapter(adapter);

        // close database
        MyDB.close();
    }

    public void getTypeOrder()
    {
        // get list view
        ListView list = findViewById(R.id.friend_list_view);

        // open database
        DBHelper MyDB = new DBHelper(this);

        // update the list
        Cursor cursor = MyDB.getOrderType(getUsername());

        // change the cursor
        adapter.changeCursor(cursor);

        // set the new adapter
        list.setAdapter(adapter);

        // close database
        MyDB.close();
    }

    public void getStatusOrder()
    {
        // get list view
        ListView list = findViewById(R.id.friend_list_view);

        // open database
        DBHelper MyDB = new DBHelper(this);

        // update the list
        Cursor cursor = MyDB.getOrderStatus(getUsername());

        // change the cursor
        adapter.changeCursor(cursor);

        // set the new adapter
        list.setAdapter(adapter);

        // close database
        MyDB.close();
    }

    public void showPopup(View v)
    {
        // creates popup
        PopupMenu popup = new PopupMenu(this, v);

        // sets onClick listener
        popup.setOnMenuItemClickListener(this);

        // inflates menu
        popup.inflate(R.menu.popup_menu);

        // show the popup
        popup.show();
    }

    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        switch(menuItem.getItemId())
        {
            case R.id.ascending_rating:
                // Toast showcasing filter
                Toast.makeText(this, "Sorted by Ascending", Toast.LENGTH_SHORT).show();

                // call function
                getIncreasingRating();

                return true;

            case R.id.descending_rating:
                // Toast showcasing filter
                Toast.makeText(this, "Sorted by Descending", Toast.LENGTH_SHORT).show();

                // call decreasing order
                getDecreasingOrder();

                return true;

            case R.id.rating_title:
                // Toast showcasing filter
                Toast.makeText(this, "Sorted by Title", Toast.LENGTH_SHORT).show();

                // call title order
                getTitleOrder();

                return true;

            case R.id.rating_status:
                // Toast showcasing filter
                Toast.makeText(this, "Sorted by Status", Toast.LENGTH_SHORT).show();

                // call status order
                getStatusOrder();

                return true;

            case R.id.rating_type:
                // Toast showcasing filter
                Toast.makeText(this, "Sorted by Type", Toast.LENGTH_SHORT).show();

                // call type order
                getTypeOrder();

                return true;

            default:
                return false;
        }
    }
}
