package com.example.tracki;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;

import java.lang.reflect.Array;
import java.util.Arrays;

public class MediaInformation extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // set content view
        setContentView(R.layout.media_information);

        // get the username
        Bundle extras = getIntent().getExtras();

        String username = "";
        if (extras.containsKey("username")) {
            username = extras.getString("username");
        }

        // creates buttons
        Button update_btn = findViewById(R.id.update_media_btn);
        ImageView home_button = findViewById(R.id.back_button_media_information);
        ImageView delete_button = findViewById(R.id.delete_media_entry);

        // variables
        TextView media_title = findViewById(R.id.media_info_title_update);
        Spinner media_type = findViewById(R.id.media_info_type_update);
        RadioGroup media_status = findViewById(R.id.media_info_progress_update);
        Spinner media_rating = findViewById(R.id.media_info_rating_update);
        TextInputEditText media_comment = findViewById(R.id.media_info_update_comment);
        TextView media_title_info = findViewById(R.id.media_title_text_info);
        TextView media_rating_text = findViewById(R.id.media_rating_text);


        // set the information from the intent
        if(extras.containsKey("title")) // for the title
        {
            // set EditText
            media_title.setText(extras.getString("title"));

            // set TextView
            media_title_info.setText(extras.getString("title"));
        }

        if (extras.containsKey("type")) // for the type
        {
            // pulls resource array
            String[] array = getResources().getStringArray(R.array.spinnerItems);

            // sets selection based on passed rating
            media_type.setSelection(Arrays.asList(array).indexOf(extras.getString("type")));
        }

        if (extras.containsKey("status")) // for the status
        {
            for (int i = 0; i < media_status.getChildCount(); i++) // finds correct radio button
            {
                View view = media_status.getChildAt(i);

                if(view instanceof RadioButton)
                {
                    RadioButton button = (RadioButton) view; // iterates through group

                    if(button.getText().toString().equals(extras.get("status"))) // if it equals the passed string
                    {
                        button.setChecked(true); // sets button to checked
                    }
                }
            }
        }

        if (extras.containsKey("rating")) // for the rating
        {
            // pulls resource array
            String[] array = getResources().getStringArray(R.array.ratingItems);

            // sets selection based on passed string
            media_rating.setSelection(Arrays.asList(array).indexOf(extras.getString("rating")));

            // sets the textView to the rating string
            media_rating_text.setText(extras.getString("rating"));
        }

        if (extras.containsKey("notes")) // for the comments
        {
            // sets the EditText View
            media_comment.setText(extras.getString("notes"));
        }


        // copy username into final
        String finalUsername = username;

        // create onClick listener for the home button
        home_button.setOnClickListener(view -> {
            Intent intent = new Intent(MediaInformation.this, MediaListActivity.class);
            intent.putExtra("username", finalUsername);
            startActivity(intent);
        });

        // create onClick listener for the deletion button
        delete_button.setOnClickListener(view -> {
            // create the database
            DBHelper MyDB = new DBHelper(this);

            AlertDialog.Builder builder = new AlertDialog.Builder(MediaInformation.this);
            builder.setMessage("Want to delete this entry?")
                    .setCancelable(true)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            // deletion code to remove account
                            MyDB.deleteMedia(finalUsername, media_title.getText().toString());

                            // starts new intent
                            Intent intent = new Intent(MediaInformation.this, MediaListActivity.class);
                            intent.putExtra("username", finalUsername);
                            startActivity(intent);
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();
                        }
                    });

            // show the alert dialogue
            AlertDialog alert = builder.create();
            alert.show();
        });

        // sets on click listener for the update button which will update the media in the database
        update_btn.setOnClickListener(view -> {
            // get all the updated values
            String new_media_title = media_title.getText().toString();
            String new_media_type = media_type.getSelectedItem().toString();

            int selectedRadioButton = media_status.getCheckedRadioButtonId();
            RadioButton selected = findViewById(selectedRadioButton);

            String new_media_status = "";

            //check radio button
            if (selected != null)
            {
                new_media_status = selected.getText().toString();
            }
            else
            {
                new_media_status = "";
            }

            // get remaining information
            String new_media_rating = media_rating.getSelectedItem().toString();
            String new_media_notes = media_comment.getText().toString();

            // create the database
            DBHelper MyDB = new DBHelper(this);

            // call the updating function
            MyDB.updateMedia(finalUsername, extras.getString("title"), new_media_title, new_media_type, new_media_status, new_media_rating, new_media_notes);

            // create intent with new information
            Intent intent = new Intent(MediaInformation.this, MediaInformation.class);
            intent.putExtra("username", finalUsername);
            intent.putExtra("title", new_media_title);
            intent.putExtra("type", new_media_type);
            intent.putExtra("status", new_media_status);
            intent.putExtra("rating", new_media_rating);
            intent.putExtra("notes", new_media_notes);
            startActivity(intent);
        });

    }

}